/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */

package arbolIG;

public class Nodos {
   
    private int dato;
    
    private Nodos izq,der;

    public Nodos(int dato, Nodos izq, Nodos der) {
       
        this.dato = dato;
        this.izq = izq;
        this.der = der;
    }

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }

    public Nodos getIzq() {
        return izq;
    }

    public void setIzq(Nodos izq) {
        this.izq = izq;
    }

    public Nodos getDer() {
        return der;
    }

    public void setDer(Nodos der) {
        this.der = der;
    }
 
}
