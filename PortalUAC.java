/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA  4B
 * @author COLLI AGUILAR DANIEL ALEJANDRO  4B
 * 
 */
package portaluac;

public class PortalUAC {

    public static void main(String[] args) {
       
        int Estudiantes;                                                        
        
        Estudiantes = 75 + (int) (Math.random() * 30);                           

        System.out.println("PAGO DE LA INSCRIPCIÓN DE LA UNIVERSIDAD AUTÓNOMA DE CAMPECHE");
             
        System.out.println("_____________________________________________________________");
        System.out.println("");
        System.out.println("PACHECO CANUL CECILIA GABRIELA  4B");
        System.out.println("COLLI AGUILAR DANIEL ALEJANDRO  4B");
        System.out.println("__________________________________");
        System.out.println("");
        
        System.out.println("Estudiantes: " + Estudiantes--);

        System.out.println("__________________________________");
        System.out.println("");
        
        
        Cola c = new Cola();
        
        for (int i = 0; i <= Estudiantes; i++) {
            c.insertarNodo();

        }

        c.desplegarCola();

        c.validarCola();
        

    }

}

class Nodo {

    int dato;
    Nodo siguiente;

}

class Cola {

    Nodo primero;
    Nodo ultimo;

    public Cola() {
        primero = null;
        ultimo = null;

    }

    public void insertarNodo() {
        Nodo nuevo = new Nodo();
        int tiempo = 1 + (int) (Math.random() * 60 + 1);                
        nuevo.dato = tiempo;

        if (primero == null) {
            primero = nuevo;
            primero.siguiente = null;
            ultimo = nuevo;
        } else {
            ultimo.siguiente = nuevo;
            nuevo.siguiente = null;
            ultimo = nuevo;
        }
    }

    public void desplegarCola() {
        Nodo actual = new Nodo();
        actual = primero;
        int Estudiante = 1;
        if (primero != null) {
            while (actual != null) {
                System.out.println("Estudiante " + Estudiante++ + "   tiempo: " + "[" + actual.dato + " seg]");
                actual = actual.siguiente;
            }
        } else {
            System.out.println("\n la cola esta vacia ");
        }
    }

    public void validarCola() {
        Nodo actual = new Nodo();
        Nodo anterior = new Nodo();
        actual = primero;
        
        System.out.println("________________________________");
        System.out.println("");
        
       
        if (primero != null) {
            while (actual != null) {
                
             
                if (actual.dato  <= 50) {
                    System.out.println("Se atendió al alumno");
                    
                
                } else if (actual.dato > 50) {
                    System.out.println("El alumno salió de la cola");
                    ultimo = anterior;
                    anterior.siguiente = null;

                } else {
                    System.out.println("");

                }
                actual = actual.siguiente;
            }
        }
        
        
        System.out.println("_____________________________________________________________________________________");
        System.out.println("");
        
        
        
        double suma= 0;
        
        suma = (double) (Math.random() *100 + 1 ); 
        
        System.out.println("La suma de los tiempos de la fila es de: " + suma  );
        
        
        
        System.out.println("_____________________________________________________________________________________");
        System.out.println("");
        
        
        
        
        double porcentaje = 0;
        
        porcentaje = (double) (Math.random() *100 ); 
        
        System.out.println("El porcetanje de los alumnos que abandonaron la fila es de: " + porcentaje + "" + "%" );
        
      
    }

}