/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */

package graph;

import java.util.ArrayList;
import java.util.List;

public class Graph {
        
      private List<Node> nodos;
      public void addNode(Node node) {
           if (nodos == null) {
           nodos = new ArrayList<>();
        }
           nodos.add(node);
      }
      public List<Node> getNodes() {
         return nodos;
      }
   @Override
   public String toString() {
   return "Graph [nodos=" + nodos + "]";
}

public static Graph getCities() {
  
    Node camp = new Node("Campeche");
    Node mex = new Node("México");
    Node tam = new Node("Tampico");
    Node cala = new Node("Calakmul");
    Node cal = new Node("Calkiní");
    Node can = new Node("Candelaria");
    Node chan = new Node("Champotón");
    Node hece = new Node("Hecechalkán");
    Node car = new Node("Carmen");
    Node esc = new Node("Escárcega");
    
    camp.addEdge(new Edge(cala,camp, 320));
    camp.addEdge(new Edge(cal, mex, 1967.3));
    camp.addEdge(new Edge(can, tam, 1202.4));
    camp.addEdge(new Edge(chan,camp, 61));
    camp.addEdge(new Edge(hece,mex,1941.5));
    camp.addEdge(new Edge(car, tam, 206));
    camp.addEdge(new Edge(esc, camp, 1106.6));
    
    Graph graph = new Graph();
    graph.addNode(camp);
    graph.addNode(mex);
    graph.addNode(tam);
    graph.addNode(cala);
    graph.addNode(cal);
    graph.addNode(can);
    graph.addNode(chan);
    graph.addNode(hece);
    graph.addNode(car);
    graph.addNode(esc);

    return graph;
}

    public static void main(String[] args) {
        Graph graph = getCities();
        System.out.println(graph);
        
        
        
    }
    
}
