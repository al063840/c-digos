/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package codigomorse;

public class CodigoMorse {

    public static void main(String[] args) {
        
        
        ClaveMorse traduccion = new ClaveMorse();
        
        
        
        String palabras = "";
        
        palabras = traduccion.traducir ("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z");
        
        System.out.println (palabras);
        
    }
    
}
