/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package arrayedad;

public class ArrayEdad {

    public static void main(String[] args) {
        
        String[] nombres = {"Estefania", "David", "Erika","William","Yazuri"};
        
         int[] edadDatos = {20,23,22,20,20};
        
        
        int[] edad = new int [5] ;
        
        edad[0]=20;
        edad[1]=23; 
        edad[2]=22; 
        edad[3]=20; 
        edad[4]=20;
        
        System.out.println("NOMBRES DE PILA");
        
         for (int i = 0; i < nombres.length; i++){
    
             System.out.println(nombres[i]);
        
    }
    
         
          System.out.println("---------------------------------------------------------");
          
          System.out.println("EDADES");
         
         
          for (int i = 0; i < edad.length; i++){
    
             System.out.println(edad[i]);
         
         
          }
        
    }
}
