/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */

package arreglodimension;

public class Arreglodimension {

    public static void main(String[] args) {
        
        
        String cm [][] = new String [12][2];
        String cmh [][] = new String [12][2];
        
        
        cm [0][0] = "CÓDIGO" ;
        cm [0][1] = "MUNICIPIO";
        cm [1][0] = "001";
        cm [1][1] = "CALKINI";
        cm [2][0] = "002";
        cm [2][1] = "CAMPECHE";
        cm [3][0] = "003";
        cm [3][1] = "CARMEN";
        cm [4][0] = "004";
        cm [4][1] = "CHAMPOTÓN";
        cm [5][0] = "005";
        cm [5][1] = "HECELCHAKÁN";
        cm [6][0] = "007";
        cm [6][1] = "HOPELCHÉN";
        cm [7][0] = "008";
        cm [7][1] = "TENABO";
        cm [8][0] = "009";
        cm [8][1] = "ESCÁRCEGA";
        cm [9][0] = "010";
        cm [9][1] = "CALAKMUL";
        cm [10][0] = "011";
        cm [10][1] = "CALENDARIA";
        cm [11][0] = "012";
        cm [11][1] = "SEYBAPLAYA";
        
        
        cmh [0][0] = "HABITANTES" ;
        cmh [0][1] = "MUNICIPIO";
        cmh [1][0] = "52890";
        cmh [1][1] = "CALKINI";
        cmh [2][0] = "259005";
        cmh [2][1] = "CAMPECHE";
        cmh [3][0] = "221094";
        cmh [3][1] = "CARMEN";
        cmh [4][0] = "83021";
        cmh [4][1] = "CHAMPOTÓN";
        cmh [5][0] = "28306";
        cmh [5][1] = "HECELCHAKÁN";
        cmh [6][0] = "37777";
        cmh [6][1] = "HOPELCHÉN";
        cmh [7][0] = "8352";
        cmh [7][1] = "TENABO";
        cmh [8][0] = "10665";
        cmh [8][1] = "ESCÁRCEGA";
        cmh [9][0] = "54184";
        cmh [9][1] = "CALAKMUL";
        cmh [10][0] = "26882";
        cmh [10][1] = "CALENDARIA";
        cmh [11][0] = "41194";
        cmh [11][1] = "SEYBAPLAYA";
       
        
     
        
        
        System.out.println ("[" + cm [0][0] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [0][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [1][0] + "]");
        System.out.println ("[" + cm [1][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [2][0] + "]");
        System.out.println ("[" + cm [2][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [3][0] + "]");
        System.out.println ("[" + cm [3][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [4][0] + "]");
        System.out.println ("[" + cm [4][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [5][0] + "]");
        System.out.println ("[" + cm [5][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [6][0] + "]");
        System.out.println ("[" + cm [6][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [7][0] + "]");
        System.out.println ("[" + cm [7][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [8][0] + "]");
        System.out.println ("[" + cm [8][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [9][0] + "]");
        System.out.println ("[" + cm [9][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [10][0] + "]");
        System.out.println ("[" + cm [10][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cm [11][0] + "]");
        System.out.println ("[" + cm [11][1] + "]");
        
        System.out.println(""); 
        
        System.out.println ("**************************************************************************");
        
        System.out.println(""); 
        
        
        System.out.println ("[" + cmh [0][0] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [0][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [1][0] + "]");
        System.out.println ("[" + cmh [1][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [2][0] + "]");
        System.out.println ("[" + cmh [2][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [3][0] + "]");
        System.out.println ("[" + cmh [3][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [4][0] + "]");
        System.out.println ("[" + cmh [4][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [5][0] + "]");
        System.out.println ("[" + cmh [5][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [6][0] + "]");
        System.out.println ("[" + cmh [6][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [7][0] + "]");
        System.out.println ("[" + cmh [7][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [8][0] + "]");
        System.out.println ("[" + cmh [8][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [9][0] + "]");
        System.out.println ("[" + cmh [9][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [10][0] + "]");
        System.out.println ("[" + cmh [10][1] + "]");
        System.out.println(""); 
        System.out.println ("[" + cmh [11][0] + "]");
        System.out.println ("[" + cmh [11][1] + "]");
        System.out.println(""); 
      
    }
        
       
}
        
        
        
        
        
        
    

