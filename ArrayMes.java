/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package arraymes;

import java.util.Scanner;

public class ArrayMes {

    public static void main(String[] args) {
        
         Scanner sc = new Scanner(System.in);
         
         int numero;
         
         String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
       
         System.out.println("Introduzca el numero del mes");
         numero = sc.nextInt();
      
      
        if (numero < 12 && numero > 0){
        
            System.out.println(meses[numero-1]);
        
    
    }
        
        
    }
    
} 
           