/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package listaenlazada;

public class Lista {
     protected Nodo inicio, fin;   
      
     public Lista(){
        inicio = null;
        fin    = null;
    }
     
    
    public void agregarAlInicio(String nombre){
        inicio = new Nodo (nombre, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.name+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    
    public int borrarDelInicio(){
        String nombre = inicio.name;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
           return 0;
        }
    
    public void agregarAlInicio1(int dato){
        inicio = new Nodo(dato, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada1(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.dato+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    // Metodo para eliminar un nodo del inicio
    public int borrarDelInicio1(){
        int dato = inicio.dato;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return dato;
    }

}
   
