/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package colaalgebraica;

import java.util.LinkedList;
import java.util.Queue;

public class ColaAlgebraica {

    public static void main(String[] args) {
        
       
        System.out.println("     "  +"COLA ALGEBRAICA");
        System.out.println(""); 
        System.out.println(""); 
        
        
        
        String cadena = "(Cadena () (()(())))";

        String cadena2 = "(Cadena )(()()))";
        
        String cadena3 = "(Cadena () (()(()))) ";
        
        String cadena4 = "(Cadena )(()())) ";
        
        String cadena5 = "(Cadena )(()())) ";

        System.out.println("(123*4)+((12-1)*(1+2))");

        System.out.println(verificaParentesis(cadena));
          
        System.out.println("(123*4)+((12-1)*+1+2))");
            
        System.out.println(verificaParentesis(cadena2));
           
        System.out.println("((pila.size()==0)&&(flag))");
            
        System.out.println(verificaParentesis(cadena3));
        
        System.out.println("((pila.size)==0)&&(flag))");
            
        System.out.println(verificaParentesis(cadena4));
        
        System.out.println("((pila.size()==0)&&(flag)");
            
        System.out.println(verificaParentesis(cadena5));
            

    }

 

    public static boolean verificaParentesis(String cadena)  {

        Queue <String> cola = new LinkedList <String>();       int i = 0;

            while (i<cadena.length()) {  

                if(cadena.charAt(i)=='(') {cola.add("(");}                                

                else if  (cadena.charAt(i)==')') {  

                            if (!cola.isEmpty()){ cola.poll(); } 

                            else { cola.add(")"); break; } 

                }

                i++;

            }

            if(cola.isEmpty()){ return true; } else { return false; }
        
        
    }
    
}
