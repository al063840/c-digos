/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package codigomorse;


public class ClaveMorse {
    
    String[] morse = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "--.--", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
	String[] palabras= {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	
	public String traducir(String dato)
	{
		String traduccion = "";
		String mayuscula = "";
		String letra = "";
		
		mayuscula = dato.toUpperCase();
		
		for(int i=1; i<=dato.length(); i++)
		{
			
                letra = mayuscula.substring(i-1, i);
			
	        for(int j=0; j<palabras.length; j++)
			{
				
	        if(letra.equals(palabras[j])){
					
                    traduccion = traduccion + morse[j] + " | ";
				
                               }
     
                        }
			
		}
		
		
		
		return traduccion;
        
        }

}
