/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package arraylistmorse;

import java.util.ArrayList;


public class ArrayListMorse {

   
    public static void main(String[] args) {
    
        ArrayList<String> letras = new ArrayList<String>();
        ArrayList<String> morse = new ArrayList<String>();
        
        letras.add("A");
        letras.add("B");
        letras.add("C");
        letras.add("D");
        letras.add("E");
        letras.add("F");
        letras.add("G");
        letras.add("H");
        letras.add("I");
        letras.add("J");
        letras.add("K");
        letras.add("L");
        letras.add("M");
        letras.add("N");
        letras.add("O");
        letras.add("P");
        letras.add("Q");
        letras.add("R");
        letras.add("S");
        letras.add("T");
        letras.add("U");
        letras.add("V");
        letras.add("W");
        letras.add("X");
        letras.add("Y");
        letras.add("Z");
        
        
        morse.add(".-");
        morse.add("-...");
        morse.add("-.-.");
        morse.add("-..");
        morse.add(".");
        morse.add("..-.");
        morse.add("--.");
        morse.add("....");
        morse.add("..");
        morse.add(".---");
        morse.add("-.-");
        morse.add(".-..");
        morse.add("--");
        morse.add("-.");
        morse.add("---");
        morse.add(".--.");
        morse.add("--.-");
        morse.add(".-.");
        morse.add("...");
        morse.add("-");
        morse.add("..-");
        morse.add("...-");
        morse.add(".--");
        morse.add("-..-");
        morse.add("-.--");
        morse.add("--..");
        
        
        System.out.println("");
        ArrayListMorse.Morse(morse, letras);
        System.out.println();
        
        
        
    }    
    public static void Morse(ArrayList <String> letras,ArrayList <String> morse){
    
    System.out.println("LETRAS DEL ALFABETO EN SU FORMA MORSE");
   
    for(int i=0;i<26;i++){
    System.out.println(morse.get(i)+" = "+letras.get(i));
        
        }
    }
}
    

