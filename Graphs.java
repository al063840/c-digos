/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */

package graph;


import java.util.ArrayList;
import java.util.List;

public class Graphs {
    
    
     private List<Node> nodos;

    public void addNodo(Node nodo) {
        if (nodos == null) {
            nodos = new ArrayList<>();
        }
        nodos.add(nodo);
    }
    
    public List<Node> getNodos() {
        return nodos;
    }

    @Override
    public String toString() {
        return "Graph{" + "nodes=" + nodos + '}';
    }  
}
