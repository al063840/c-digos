package administracione;

/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 3B
 */


import java.util.InputMismatchException;
import java.util.Scanner;


public class AdministracionE {

    
     private static int num1, num2,num3, num4,num5, num6,resultado1,resultado2,resultado3;
     private static boolean error;
    
  
     public static void main(String[] args)  {
       
     
         Scanner teclado = new Scanner (System.in);
          error = false; 
        
    try{
        
    System.out.println("Ingresar el numero 1");
    num1=teclado.nextInt();
    System.out.println("Ingresar el numero 2");
    num2=teclado.nextInt();
    System.out.println("Ingresar el numero 3");
    num3=teclado.nextInt();
    System.out.println("Ingresar el numero 4");
    num4=teclado.nextInt();
    System.out.println("Ingresar el numero 5");
    num5=teclado.nextInt();
    System.out.println("Ingresar el numero 6");
    num6=teclado.nextInt();
    
    
    
    resultado1=num1/num2;
    System.out.println("El resultado de num1 y num2 es:"+ resultado1 );
    resultado2=num3/num4;
    System.out.println("El resultado de num3 y num4 es:"+ resultado2 );
    resultado3=num5/num6;
    System.out.println("El resultado de num5 y num6 es:"+ resultado3 );
    
    }
   
  
    catch(InputMismatchException in){
    System.err.println("Lo ingresado es incorrecto");
    }
            
     
    catch(ArithmeticException e)
    {
    
         error=true;
         System.err.println("Se produjo un error de tipo:" + e);
    
   
     }finally{
    
        if(error==true)
           System.out.println("Se produjo un error en una de las operaciones");
    
    else
            System.out.println("El código se ejecutó satisfactoriamente ");
       
      
        
            }
     }

   
    }

  
    

