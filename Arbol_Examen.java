/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package arbol_examen;


public class Arbol_Examen {

   
    public static void main(String[] args) {
       
        int dato;
        String nombre;
       
        ArbolBinario_Examen miArbol = new ArbolBinario_Examen();
        
        miArbol.agregarNodo(80, "P");
        miArbol.agregarNodo(75, "H");
        miArbol.agregarNodo(71, "G");
        miArbol.agregarNodo(66, "B");
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(85, "U");
        miArbol.agregarNodo(82, "R");
       

        
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()) {
            miArbol.preOrden(miArbol.raiz);
    }
    
}
}
