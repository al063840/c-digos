/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package codigoascii;

import javax.swing.JOptionPane;

public class CodigoAscii {

    public static void main(String[] args) {
       
       
		String data = JOptionPane.showInputDialog("Input code ascii");
		
		int d = Integer.parseInt(data);
		char c = (char)(d);
		System.out.println(data + " = " + c);
        
        
        
    }
    
}
