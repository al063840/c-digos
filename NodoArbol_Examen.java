/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol_examen;

/**
 *
 * @author personal
 */
public class NodoArbol_Examen {
    
     int dato;
    String nombre;
    NodoArbol_Examen hijoizquierdo, hijoderecho;
    public NodoArbol_Examen (int d, String s){
        this.dato = d;
        this.nombre = s;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
    public String toString(){
        return nombre + "El dato es : "+dato;
    
    }
}
