/**
 * @author UNIVERSIDAD AUTONOMA DE CAMPECHE
 * @author ISC
 * @author PACHECO CANUL CECILIA GABRIELA 4B
 */
package listaenlazada;


public class ListaEnlazada {

    
    public static void main(String[] args) {
       
        
        Lista listaenlazada = new Lista();
        listaenlazada.agregarAlInicio("Estefania");
        listaenlazada.agregarAlInicio("Jose");
        listaenlazada.agregarAlInicio("Eduardo");
        listaenlazada.agregarAlInicio("William");
        listaenlazada.agregarAlInicio("Daniel");
        listaenlazada.agregarAlInicio("Yazuri ");
        listaenlazada.agregarAlInicio("Josue");
        listaenlazada.agregarAlInicio("Ricardo");
        listaenlazada.agregarAlInicio("David");
        listaenlazada.agregarAlInicio("Erika");
        listaenlazada.mostrarListaEnlazada();
        listaenlazada.borrarDelInicio();
        listaenlazada.mostrarListaEnlazada();
        
        Lista lista = new Lista();
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.agregarAlInicio("20");
        listaenlazada.mostrarListaEnlazada();
        listaenlazada.borrarDelInicio();
        listaenlazada.mostrarListaEnlazada();
    }
    
}
